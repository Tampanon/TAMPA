# Tampa's Awesome Map Placement Assistant (TAMPA)

A simple bash utility to fill out map templates via a coordinate system.

[![ezgif-7-67914bf77e87.gif](https://i.postimg.cc/tR65pgDK/ezgif-7-67914bf77e87.gif)](https://postimg.cc/XXVFg4RL)  

# Introduction

The purpose of this simple bash script is to add support for certain regional map templates, and fill out the templates accurately for each region collected by the user. The flags collected come from the [Extra Flags for 4chan](https://gitlab.com/flagtism/Extra-Flags-for-4chan) project and repository. The coordinates for this script are stored in a simple plaintext file "listofcoordinates.txt" within each region's directory, and compared against the aforementioned repo's directory-specific catalogue "flag_list.txt". Coordinates for the timebeing will have to be crowdsourced, therefore the best way to contribute to the project at the moment is by submitting coordinates for templates you'd like to see supported.

# Features

*  Capable of filling in collected/uncollected regions, coloring in backgrounds, as well as placing flags
*  Automatic updates from latest upstream changes
*  Functional design to facilitate universal template compatibility
*  User-selectable colors cross-referenced with the ImageMagick library which can be entered by name, HEX, or RGB
*  Verbose updates for simple debugging

# Supported Maps

*  United States
    *  Including state-level maps for Alabama through New Jersey, alphabetically, excluding Alaska (incomplete set in repo)
*  Argentina
*  Austria
*  Canada (submitted by Sonora)
*  Chile
*  Hungary
*  Italy (submitted by Trieste)
*  Latvia
*  Mexico (submitted by Sonora)
*  Nicaragua
*  Norway
*  Romania
*  Spain
*  Switzerland

This list will continue to expand as time goes on and more coordinates are added. The majority of maps used were made by OPLK, Minnie, or Montgomery, and TAMPA wouldn't be possible without their contributions to /flag/.  

*Note: Unfortunately, I've had to remove stamps added to the upper-left corner of map templates, used to denote the creator. This interferes with the script's ability to color-in backgrounds. If you're a map maker and would like credit, or a stamp added elsewhere, please let me know.*

# Requirements

If you need help meeting any of these requirements, feel free to ask on /flag/ or open an issue on the repo.

*  Ability to run bash files. Linux or MacOS is recommended, but it's still easily doable on Windows.
*  [git](https://git-scm.com/) is required for version control and automatic updates.
*  [Imagemagick](https://imagemagick.org/index.php) must be installed, or the script won't be able to interface with images.
*  [bc](https://www.gnu.org/software/bc/manual/html_mono/bc.html) is required if you wish to view proper collection percentages. This can be easily installed with most Linux package managers, on Mac/Windows your mileage may vary.
*  A /flag/-compliant collection based on the work at the previously-mentioned repo

# Usage

If you're running Windows, please follow the instructions [here](https://gitlab.com/Tampanon/TAMPA/-/wikis/How-to-Use-on-Windows-Systems).

1.  Clone this directory to a safe location on your computer using the following terminal command: `git clone https://gitlab.com/Tampanon/TAMPA`  
2.  Enter the cloned directory: `cd TAMPA`  
3.  Make the script executable: `chmod +x TAMPA.sh` (on Mac/Windows this may vary slightly)  
4.  Add your collected flags to the script directory following the proper folder structure. If/when prompted to merge folders, select yes. Because this project is still faily new, I recommend ***copying*** your supported flags into the cloned directory, in order to protect your original collection from any potential harm. Note: All supported templates are automatically added to the proper directory when cloning the repo, your collected regionals will need to be merged/placed into these folders.
5.  **I strongly suggest visiting the "Folder Structure" section below before continuing in order to ensure the script runs properly.**  
6.  Run the script: `./TAMPA.sh` (on Mac/Windows this may vary slightly)

![image](https://i.postimg.cc/HLcpcPPb/untitled.gif)  

Upon script completion, check your maps.

# Folder Structure

Folder hierarchy must follow this structure: Tampas-Awesome-Map-Placement-Assistant/(Collected National Flags)/(Collected 1st-Level Flags)/(Collected 2nd-Level Flags)/etc., *e.g. Tampas-Awesome-Map-Placement-Assistant/United States/Florida/Hillsborough County*  
  
[![image.png](https://i.postimg.cc/13YrRZQV/image.png)](https://postimg.cc/yDcZntW7)  

If folder structure confuses you, the script also contains native support for the structure used by the Extra-flags-for-4chan repository (automatically cloned by the script), which you can view as an example. 

# Troubleshooting

Improperly-filled maps can be the result of one of the following:
*  If you have just cloned the directory and are running the script for the first time, **it will mess up** and fill in all possible regions regardless of your collection. Don't worry, when the script completes, simply run it again and the errors should be fixed. **Please note that this only happens on the first run**, I am still looking into the culprit.
*  Your folder directory is improperly setup.
*  Your folder/file names do not accurately line up with the provided names in "flag_list.txt", *e.g. You have a folder/file named "Ostfold" instead of "Østfold"*.
*  Your map templates are not placed in the directory containing your regionals.
*  Your map templates are not named in the format "RegionalMap.png"
*  You're attempting to fill out a map which has not yet been officially supported, or has had its dimensions changed. All supported maps are in this repo, and can also be found in the [Extra-Flags-for-4chan](https://gitlab.com/flagtism/Extra-Flags-for-4chan/tree/master/maps) repo. If you would like to see support for a new template, consider supporting the project by contributing coordinates for a map found at the most recent link.

# Coordinate Requirements

Coordinates must be contained within a .txt file, with a newline separating each coordinate pair. **These coordinates must be in alphabetical order**. An important distinction to make is that the coordinates must be in a character-based alphabetic ordering, vs. a word-based alphabetic ordering.  

*e.g. "Santa Rosa County" should be before "St. Johns County", even though "Saint" technically comes before "Santa".*  

![image](https://i.postimg.cc/C1CB5Z3D/image.png)  
  
As shown above, the left panel contains "flag_list.txt" which is a plaintext file listing all supported regions of a template in character-based alphabetic order. The right panel contains "listofcoordinates.txt", another plaintext file which contains the specific (x,y) coordinates for the center of each region within a template. The *nth* line of "listofcoordinates.txt" represents the *nth* line of "flag_list.txt".  
  
*Please note that in the image above, the line numbers are not part of the original files, e.g. the first line for each file should read "Alachua County" and "1302,441" respectively.*

For a step-by-step guide on creating proper coordinate lists, please visit [this](https://gitlab.com/Tampanon/Tampas-Awesome-Map-Placement-Assistant/-/wikis/Coordinate-Creation-Guide) page.


# Future Plans

User configuration files for using certain colors on certain countries, command-line argument support for invoking more well-defined options, statistics collection shown to user upon script completion, and increasing script optimization/legibility. If you have any other ideas or suggestions, please create a new issue or reach out to Tampa on /flag/