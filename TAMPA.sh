#!/bin/bash

# Welcome screen and initial notes to user (may add support for most-recent changelog in future)
cat << "Startup"

8888888 8888888888    .8.                   ,8.       ,8.          8 888888888o        . 8.          
      8 8888          .888.                 ,888.     ,888.         8 8888    `88.     .888.         
      8 8888         .88888.               .`8888.   .`8888.        8 8888     `88    :88888.        
      8 8888        . `88888.             ,8.`8888. ,8.`8888.       8 8888     ,88   . `88888.       
      8 8888       .8. `88888.           ,8'8.`8888,8^8.`8888.      8 8888.   ,88'  .8. `88888.      
      8 8888      .8`8. `88888.         ,8' `8.`8888' `8.`8888.     8 888888888P'  .8`8. `88888.     
      8 8888     .8' `8. `88888.       ,8'   `8.`88'   `8.`8888.    8 8888        .8' `8. `88888.    
      8 8888    .8'   `8. `88888.     ,8'     `8.`'     `8.`8888.   8 8888       .8'   `8. `88888.   
      8 8888   .888888888. `88888.   ,8'       `8        `8.`8888.  8 8888      .888888888. `88888.  
      8 8888   .888888888. `88888.   ,8'       `8        `8.`8888.  8 8888      .888888888. `88888.  

Version 2.0

Startup

echo "Welcome to TAMPA! Please familiarize yourself with documentation and proper folder structure before use."
echo "WARNING: Be careful running this script in your primary flag directory. The script is provided as-is, and I'm not responsible for negligence on your part."
echo
echo "==================================================="
echo "                   CHANGELOG                       "
echo "==================================================="
echo "  * Added support for New Mexico and New York"
echo "==================================================="
echo
echo "The script is sleeping for 10 seconds, and will begin shortly."

sleep 10

# Built-in self-update via git
echo Updating TAMPA with changes made to upstream.
git fetch --all
git reset --hard origin/master
git pull

# Clone remote flag repository for comparison of flags
git clone https://gitlab.com/flagtism/Extra-Flags-for-4chan.git
cd Extra-Flags-for-4chan
git pull origin master
cd ..

# Ask user which color they'd like to use for collected regions
echo "We will now set your preferred color for collected regions."
echo "You can enter the name of the color, its HEX value, or its RGB."
echo "For a full list of officially supported colors, please visit (https://imagemagick.org/script/color.php)"
read -p "Please enter the color you'd like to use for collected regions: " primaryColor

# Ask user if they wish to color uncollected regions a separate color
while true; do
	read -p "Would you like to color uncollected regions a second color? (Y/N) " promptOne
	case $promptOne in
		[Yy]* ) secondColorBool=$((secondColorBool=1)); break;;
		[Nn]* ) secondColorBool=$((secondColorBool=0)); break;;
		* ) echo "Please answer yes (Y) or no (N).";;
	esac
done

# If user wishes to fill uncollected regions with a separate color
if [ "$secondColorBool" == 1 ]; then
	read -p "Please enter the color you'd like to use for uncollected regions: " secondaryColor
fi

# Ask user if they wish to change the map's background color
while true; do
	read -p "Would you like to change the template's background color? (Y/N) " promptTwo
	case $promptTwo in
		[Yy]* ) backgroundBool=$((backgroundBool=1)); break;;
		[Nn]* ) backgroundBool=$((backgroundBool=0)); break;;
		* ) echo "Please answer yes (Y) or no (N).";;
	esac
done

# If user wishes to change template background
if [ "$backgroundBool" == 1 ]; then
	read -p "Please enter the color you'd like to use for the template background: " backgroundColor
fi

# Ask user if they wish to place collected flags onto map template
while true; do
	read -p "Would you like to place collected flags onto the map template (Y/N) " promptThree
	case $promptThree in
		[Yy]* ) placeFlagBool=$((placeFlagBool=1)); break;;
		[Nn]* ) placeFlagBool=$((placeFlagBool=0)); break;;
		* ) echo "Please answer yes (Y) or no (N).";;
	esac
done

# Ask user if they wish to place badges onto map template
while true; do
	read -p "Would you like to place a badge/emblem/CoA onto the map template (Y/N) " promptSix
	case $promptSix in
		[Yy]* ) placeBadgeBool=$((placeBadgeBool=1)); break;;
		[Nn]* ) placeBadgeBool=$((placeBadgeBool=0)); break;;
		* ) echo "Please answer yes (Y) or no (N).";;
	esac
done

# Declaration of arrays for supported maps, the regionals/coordinates within their directories, and housekeeping variables
declare -a currentMap=("United States" "Alabama" "Arizona" "Arkansas" "California" "Colorado"
"Connecticut" "Delaware" "Florida" "Georgia" "Hawaii" "Idaho" "Illinois" "Indiana" "Iowa" "Louisiana" "Maine" "Maryland" "Massachusetts" "Michigan" "Minnesota" "Mississippi" "Missouri" "Montana" "Nebraska" "Nevada" "New Hampshire" "New Jersey" "New Mexico" "New York" "Argentina" "Austria" "Canada" "Chile" "Hungary" "Italy" "Latvia" "Mexico" "Netherlands" "Nicaragua" "Norway" "Romania" "Spain" "Switzerland")
declare -a currentRegional=()
declare -a firstLevelMaps=("United States" "Argentina" "Austria" "Canada" "Chile" "Hungary" "Italy" "Latvia" "Mexico" "Netherlands" "Nicaragua" "Norway" "Romania" "Spain" "Switzerland")
declare -a secondLevelMaps=("Alabama" "Arizona" "Arkansas" "California" "Colorado" "Connecticut"
"Delaware" "Florida" "Georgia" "Hawaii" "Idaho" "Illinois" "Indiana" "Iowa" "Louisiana" "Maine" "Maryland" "Massachussetts" "Michigan" "Minnesota" "Mississippi" "Missouri" "Montana" "Nebraska" "Nevada" "New Hampshire" "New Jersey" "New Mexico" "New York")
declare -a currentCoordinates=()
declare -a currentOffset=()
declare -a remoteRegional=()
declare -A unitedStatesArray=([Alabama]=1 [Alaska]=1 [Arizona]=1 [Arkansas]=1 [California]=1 [Colorado]=1 [Connecticut]=1 [Delaware]=1 [Florida]=1 [Georgia]=1 [Hawaii]=1 [Idaho]=1 [Illinois]=1 [Indiana]=1 [Iowa]=1 [Kansas]=1 [Kentucky]=1 [Louisiana]=1 [Maine]=1 [Maryland]=1 [Massachusetts]=1 [Michigan]=1 [Minnesota]=1 [Mississippi]=1 [Missouri]=1 [Montana]=1 [Nebraska]=1 [Nevada]=1 [New Hampshire]=1 [New Jersey]=1 [New Mexico]=1 [New York]=1 [North Carolina]=1 [North Dakota]=1 [Ohio]=1 [Oklahoma]=1 [Oregon]=1 [Pennsylvania]=1 [Rhode Island]=1 [South Carolina]=1 [South Dakota]=1 [Tennessee]=1 [Texas]=1 [Utah]=1 [Vermont]=1 [Virginia]=1 [Washington]=1 [West Virginia]=1 [Wisconsin]=1 [Wyoming]=1)

# Ask user if they wish to fill out all maps
while true; do
	read -p "Would you like to fill out all supported maps? This could take a while. (Y/N) " promptFour
	case $promptFour in
		[Yy]* ) fillOutAllMaps=$((fillOutAllMaps=1)); break;;
		[Nn]* ) fillOutAllMaps=$((fillOutAllMaps=0)); break;;
		* ) echo "Please answer yes (Y) or no (N).";;
	esac
done

# Loop to iterate through each map
for i in "${currentMap[@]}"; do
	# If user doesn't want to fill out all maps, ask before each map whether or not to proceed with operations
	if [[ "$fillOutAllMaps" == 0 ]]; then
		while true; do
			read -p "Would you like to fill out the ${currentMap[$mapIncrementer]} map? (Y/N) " promptFive
			case $promptFive in
				[Yy]* ) skipMap=$((skipMap=0)); break;;
				[Nn]* ) skipMap=$((skipMap=1)); break;;
				* ) echo "Please answer yes (Y) or no (N).";;
			esac
		done

		# If user wishes to skip, update housekeeping variables as-needed
		if [[ "$skipMap" == 1 ]]; then
			coordinatesIncrementer=$((coordinatesIncrementer=0));
			regionalIncrementer=$((regionalIncrementer=0));
			remoteIncrementer=$((remoteIncrementer=0));
			currentIncrementer=$((currentIncrementer=0));
			flagMatches=$((flagMatches=0));
		fi
	elif [[ "$fillOutAllMaps" == 1 ]]; then
		skipMap=$((skipMap=0));
	fi

	# This section may not work, check out "countryName" logic
	if [[ "$skipMap" == 0 ]]; then
		if [[ "$i" == "United States" ]] || [[ "$i" == "Argentina" ]] || [[ "$i" == "Austria" ]] || [[ "$i" == "Canada" ]] || [[ "$i" == "Chile" ]] || [[ "$i" == "Hungary" ]] || [[ "$i" == "Italy" ]] || [[ "$i" == "Latvia" ]] || [[ "$i" == "Mexico" ]] || [[ "$i" == "Netherlands" ]] || [[ "$i" == "Nicaragua" ]] || [[ "$i" == "Norway" ]] || [[ "$i" == "Romania" ]] || [[ "$i" == "Spain" ]] || [[ "$i" == "Switzerland" ]]; then
			firstLevelBool=1
			secondLevelBool=0
			countryName="$i"
		elif [[ ${unitedStatesArray["${currentMap[$mapIncrementer]}"]} ]]; then
			secondLevelBool=1
			firstLevelBool=0
			countryName="United States"/"${currentMap[$mapIncrementer]}"
		fi
		
		# This set of statements copies all relevant flag_list.txt files from cloned git repo for comparison purposes
		if [[ "${currentMap[$mapIncrementer]}" == "United States" ]]; then
			cp Extra-Flags-for-4chan/flags/United\ States/flag_list.txt "$countryName"/.flag_list.txt
		elif [[ "$secondLevelBool" == 1 ]]; then
			cp Extra-Flags-for-4chan/flags/United\ States/"${currentMap[$mapIncrementer]}"/flag_list.txt "$countryName"/.flag_list.txt
		elif [[ "$secondLevelBool" == 0 ]]; then
			cp Extra-Flags-for-4chan/flags/"${currentMap[$mapIncrementer]}"/flag_list.txt "$countryName"/.flag_list.txt
		fi

		# If not skipping current map, store all folders and files (without extension) to a .txt in country's directory
		ls "$countryName" -1 | sed -e 's/\.png$//' > "$countryName"/.listofflags.txt
		# Store each line of respective .txt files to a respective array of strings
		mapfile -t currentRegional < "$countryName"/.listofflags.txt
		mapfile -t currentCoordinates < "$countryName"/.listofcoordinates.txt
		mapfile -t currentOffset < "$countryName"/.listofcoordinates.txt
		mapfile -t remoteRegional < "$countryName"/.flag_list.txt

		offset_list=""
		while read value; do
			if [[ -z $offset_list ]]; then
				offset_list="$value"
			else
				offset_list="$offset_list,$value"
			fi
		done < "$countryName"/$coordinatesFile

		for index in ${!currentOffset[@]}; do
			value=${currentOffset[$index]}
			x=${value%%,*}
			y=${value##*,}
			currentOffset[$index]="$((x-8)),$((y-5))"
		done

		# Logic to convert (x,y) coordinate to +x+y coordinates required for geometry argument
		currentOffset=("${currentOffset[@]/#/+}")
		currentOffset=("${currentOffset[@]/,/+}")

		# If not skipping current map,
		if [[ "$skipMap" == 0 ]]; then
			# Loop to read git repo flag_list.txt
			while read p;
			do
				# Compare local flags to remote flag list
				for l in "${remoteRegional[$remoteIncrementer]}"; do
					# If a flag in the git repo is found in local collection,
					if grep -h -q "${remoteRegional[$remoteIncrementer]}" "$countryName"/.listofflags.txt > /dev/null; then
						totalCollectedFlags=$((totalCollectedFlags+1));
						# Locate its coordinate values within the coordinate array
						for c in "${currentCoordinates[$coordinatesIncrementer]}"; do
							echo "${remoteRegional[$remoteIncrementer]}" is present, coloring in at coordinates "${currentCoordinates[$coordinatesIncrementer]}"
							# Fill in corresponding region at the supplied coordinates on the supported map template
							convert "$countryName"/"${currentMap[$mapIncrementer]}"Map.png -fuzz 01% -fill $primaryColor -draw "color ${currentCoordinates[$coordinatesIncrementer]} floodfill" "$countryName"/"${currentMap[$mapIncrementer]}"Map.png
							if [ "$placeFlagBool" == 1 ]; then
								# If the present within the directory and not in its own specific folder, place it on the template
								if test -f "$countryName"/"${remoteRegional[$remoteIncrementer]}".png; then
									convert "$countryName"/"${currentMap[$mapIncrementer]}"Map.png "$countryName"/"${remoteRegional[$remoteIncrementer]}".png -geometry "${currentOffset[$remoteIncrementer]}" -composite "$countryName"/"${currentMap[$mapIncrementer]}"Map.png
								# If the collected regional is contained within a folder, place it on the template
							else
								convert "$countryName"/"${currentMap[$mapIncrementer]}"Map.png "$countryName"/"${remoteRegional[$remoteIncrementer]}"/"${remoteRegional[$remoteIncrementer]}".png -geometry "${currentOffset[$remoteIncrementer]}" -composite "$countryName"/"${currentMap[$mapIncrementer]}"Map.png
							fi
					fi
					currentFlagMatches=$((currentFlagMatches+1));
					flagMatches=$((flagMatches+1));
					echo "$flagMatches" flag matches so far
				done
			elif [ "$secondColorBool" == 1 ]; then
				for c in "${currentCoordinates[$coordinatesIncrementer]}"; do
					echo "${remoteRegional[$remoteIncrementer]}" is not present, marking missing at coordinates "${currentCoordinates[$coordinatesIncrementer]}"
					convert "$countryName"/"${currentMap[$mapIncrementer]}"Map.png -fuzz 01% -fill "$secondaryColor" -draw "color ${currentCoordinates[$coordinatesIncrementer]} floodfill" "$countryName"/"${currentMap[$mapIncrementer]}"Map.png
					totalUncollectedFlags=$((totalUncollectedFlags+1));
					currentUncollectedFlags=$((currentUncollectedFlags+1));
				done
				# Otherwise if flag in git repo is not found in the local collection,
			else
				# Print lack of presence to user and continue on
				totalUncollectedFlags=$((totalUncollectedFlags+1));
				currentUncollectedFlags=$((currentUncollectedFlags+1));
				echo "${remoteRegional[$remoteIncrementer]}" not found, moving on
					fi
			# Increment counters
			regionalIncrementer=$((regionalIncrementer+1));
			coordinatesIncrementer=$((coordinatesIncrementer+1));
			remoteIncrementer=$((remoteIncrementer+1));
		done
	done < "$countryName"/.flag_list.txt
	
	if [ "$placeBadgeBool" == 1 ]; then
		resizeLine="$(grep "${currentMap[$mapIncrementer]}Resize" .badges/badgecoordinates)"
		resizeValue="${resizeLine#*=}"
		badgeLine="$(grep "${currentMap[$mapIncrementer]}Coords" .badges/badgecoordinates)"
		badgeCoordinates="${badgeLine#*=}"
		convert "$countryName"/"${currentMap[$mapIncrementer]}"Map.png -gravity West -extent "$resizeValue" "$countryName"/"${currentMap[$mapIncrementer]}"Map.png
		convert "$countryName"/"${currentMap[$mapIncrementer]}"Map.png .badges/"${currentMap[$mapIncrementer]}"Badge.png -geometry $badgeCoordinates -composite "$countryName"/"${currentMap[$mapIncrementer]}"Map.png
	fi
	
	if [[ "$backgroundBool" == 1 ]]; then
		echo Coloring in background...
		convert -quiet "$countryName/${currentMap[$mapIncrementer]}"Map.png -fuzz 01% -fill $backgroundColor -draw "color 1,1 floodfill" "$countryName/${currentMap[$mapIncrementer]}"Map.png
	fi

	# Update user of last-completed map, reset template-specific counters, and move to next supported directory
	currentTotalFlags=$((currentFlagMatches+currentUncollectedFlags));
	currentPercentCollected=$(bc -l <<< "scale=2; $currentFlagMatches / $currentTotalFlags")
	currentPercentCollected=${currentPercentCollected#"."}
	echo Work for the map of "${currentMap[$mapIncrementer]}" has been completed.
	echo =============================================================================================
	echo You have collected "$currentFlagMatches" regions of "${currentMap[$mapIncrementer]}", out of "$currentTotalFlags" total!
	echo This means your "${currentMap[$mapIncrementer]}" collection is $currentPercentCollected% complete!
	echo =============================================================================================
	regionalIncrementer=$((regionalIncrementer=0));
	coordinatesIncrementer=$((coordinatesIncrementer=0));
	remoteIncrementer=$((remoteIncrementer=0));
	currentIncrementer=$((currentIncrementer=0));
	currentFlagMatches=$((currentFlagMatches=0));
	currentUncollectedFlags=$((currentUncollectedFlags=0));
	currentTotalFlags=$((currentTotalFlags=0));
	currentPercentCollected=$((currentPercentCollected=0));
	flagMatches=$((flagMatches=0));
		fi
	
	fi
	if [[ "$skipMap" == "0" ]]; then
		mapIncrementer=$((mapIncrementer+1));
	fi
	if [[ "$skipMap" == "1" ]]; then
		mapIncrementer=$((mapIncrementer+1));
	fi
done

totalOverallFlags=$((totalUncollectedFlags+totalCollectedFlags));
percentCollected=$(bc -l <<< "scale=2; $totalCollectedFlags / $totalOverallFlags")
percentCollected=${percentCollected#"."}
echo All maps updated.
echo =============================================================================================
echo You have collected $totalCollectedFlags regions overall, out of $totalOverallFlags selected!
echo This means your collection of selected regions is $percentCollected% complete!
echo =============================================================================================
echo Script complete.

